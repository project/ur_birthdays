<?php
/**
 * @file
 * Administer UR Birthday settings.
 * @ingroup forms
 */

/**
 * System settings form.
 */
function ur_birthdays_admin_settings() {
  // Load relationship types.
  $options = user_relationships_types_load();
  // Now make an array of just names.
  $names = array();
  foreach ($options as $relation_names) {
    $names[$relation_names->rtid] = $relation_names->name;
  }

  $form['ur_birthdays_active_relationships'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Choose active birthday relationships'),
    '#options' => $names,
    '#default_value' => variable_get('ur_birthdays_active_relationships', array()),
    '#description' => t('Check which types of relationships should trigger a notification display via the birthdays block'),
  );

  // Get a list of our cck date fields.
  $datefields = array();
  $sql = 'SELECT field_name FROM {content_node_field} WHERE type= "datetime"';
  $theresult = db_query($sql);
  while ($data = db_fetch_object($theresult)) {
    $datefields[$data->field_name] = $data->field_name;
  }

  $form['ur_birthdays_cck_field'] = array(
    '#type' => 'select',
    '#title' => t('Choose the CCK Birthday Field'),
    '#options' => $datefields,
    '#default_value' => variable_get('ur_birthdays_cck_field', ''),
    '#description' => t('Choose which CCK field is used for your birthdays.  Currently limited to datetime fields.'),
  );

  // Get a list of our cotent types.
  $node_types = node_get_types('names');
  $form['ur_birthdays_node_type'] = array(
    '#type' => 'select',
    '#title' => t('Choose your User Profile Content Type'),
    '#options' => $node_types,
    '#default_value' => variable_get('ur_birthdays_node_type', 'profile'),
    '#description' => t('Choose your Content Profile node type'),
  );

  return system_settings_form($form);
}
